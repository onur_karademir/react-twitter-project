import React, { useState } from "react";
import "./App.css";
import Twitter from "./components/Twitter";
import Form from "./components/Form";
import Header from "./components/Header";
import Content from "./components/Content";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/content" exact component={Content}></Route>
      </Switch>
      <Redirect to="/"></Redirect>
    </BrowserRouter>
  );
}
const Home = () => {
  const [message, setMessage] = useState("");
  const [toggleClass , setToggleClass] = useState("hidden")
  const [users, setUser] = useState([
    { name: "Onur", post: "Hello my frends.",date:"02.09.2020",time:"06:20:13" },
    { name: "Merve", post: "Faruk pharmacy ?? Did you mean faruk coffee house ?? ",date:"02.09.2020",time:"15:30:45" },
    { name: "Kaan", post: "ı am at work.",date:"02.09.2020",time:"17:15:25" },
  ]);
  const postHendler = (name, post,date,time) => {
    if (name === "" || post === "") {
      setMessage("bütün alanları doldurmalısın");
      setToggleClass("show");
      setTimeout(() => {
        setToggleClass("hidden")
      }, 2000);
    } else {
      setMessage("gönderildi :)");
      setToggleClass("show");
      setTimeout(() => {
        setToggleClass("hidden")
      }, 2500);
      let neoUser = {
        name: name,
        post: post,
        date:date,
        time:time
      };
      let hendlerUser = [...users, neoUser];
      setUser(hendlerUser);
      console.log(hendlerUser);
    }
    
  };

  return (
    <div className="container twitter-container p-3">
      <Form postHendler={postHendler}></Form>
      <p className={`alert-message ${toggleClass}`}>{message}</p>
      {users.map((user) => (
        <Twitter time={user.time} date={user.date} name={user.name} post={user.post}></Twitter>
      ))}
    </div>
  );
};

export default App;
