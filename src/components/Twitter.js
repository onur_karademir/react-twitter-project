import React from "react";

function Twitter({name,post,date,time}) {
  return (
    <div className="twit-container mx-auto d-block text-center bg-white p-3 mt-2 mb-2 rounded">
      <h1>{name}</h1>
      <h4>{post}</h4>
      <h5>{date}  {time}</h5>
    </div>
  );
}

export default Twitter;
