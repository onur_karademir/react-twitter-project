import React from "react";
import { NavLink } from "react-router-dom";
import icon from "../images/a.png";
const Header = () => {
  return (
    <nav className="navbar navbar-expand-md twitter-header container">
      <div className="container">
      <NavLink exact className="navbar-brand" to="/">
      <img src={icon} className="img-responsive twitter-icon" alt="Image"></img>
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <NavLink exact className="nav-link" to="/content">
              Content
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink exact className="nav-item nav-link" to="/about">
              About
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink exact className="nav-item nav-link" to="/contact">
              Contact
            </NavLink>
          </li>
        </ul>
      </div>
      </div>
    </nav>
  );
};

export default Header;
