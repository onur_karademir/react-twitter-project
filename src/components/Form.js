import React, { Component } from 'react';


class Form extends Component {
    state = { 
        name:"",
        post:"" 
     }
     submitHendler = (e) => {
        e.preventDefault();
        const date = new Date().toLocaleDateString();
        const time = new Date().toLocaleTimeString();
        this.props.postHendler(this.state.name,this.state.post,date,time)
        this.setState({
            name:"",
            post:""
        })
     }
     inputHendler = (e) => {
        this.setState({
            name:e.target.value
        })
     }
     textHendler = (e) => {
        this.setState({
            post:e.target.value
        })
     }
    render() { 
        return ( 
            <div className="form-group twitter-form">
             <form onSubmit={this.submitHendler}>
                <input className="form-control mb-2" value={this.state.name} onChange={this.inputHendler} type="text" placeholder="Name"/>
                <textarea className="form-control" value={this.state.post} onChange={this.textHendler} cols="30" rows="3" placeholder="What's happening"></textarea>
                <button className="btn btn-light twitter-button mt-3 mx-auto d-block">Send<i className="fa fa-paper-plane ml-1"></i></button>
            </form>
            </div>
            
         );
    }
}
 
export default Form;