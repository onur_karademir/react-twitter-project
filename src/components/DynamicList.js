import React, { Component } from 'react';

class DynamicLİst extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            buyItem :["egg","apple","lime"]
         }
    }
    itemAdd (e) {
            e.preventDefault();
            const {buyItem} = this.state;
            const newItem = this.newItem.value;
            this.setState({
                buyItem : [...this.state.buyItem,newItem]
            })
            this.addForm.reset();
    };

    removeItem(item) {
        const newBuyItems = this.state.buyItem.filter(buyItems=>{
            return buyItems !== item
        }) 
        this.setState({
            buyItem : [...newBuyItems]
        })
    }

    render() { 
        return ( 
            <div>
                <form ref={(input)=>this.addForm=input} onSubmit={(e)=>{this.itemAdd(e)}}>
                <input ref={(item)=>{this.newItem = item}} className="form-control mb-2" type="text" placeholder="Name"/>
                <button>add item</button>
            </form>
                {this.state.buyItem.map(item=> {
                    return (
                        <div>
                      <p key={item}>{item}</p>
                      <button onClick={(e)=> this.removeItem(item)} className="btn text-danger">delete</button>
                        </div>
                    )
                })}
               
            </div>
            
         );
    }
}
 
export default DynamicLİst;